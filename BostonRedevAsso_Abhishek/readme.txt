I have used the existing HTML, CSS, and JavaScript to keep certain part of the webpage functioning as before.
To alter the search mechanism, I have created few JavaScript and CSS files of my own to support those funstionalities.

My approach:
1. I wanted to make sure that only those criteria are visible to the user that he is looking and aware of. 
2. So in order to do that I created a dropdown box that will enable the user to select the options that he is familiar        with.
3. Only the DIVs of the selected options are being made visible to the user.
4. I have maintained the original format of the search engine on the left hand side of the page, this provides user the flexibility to user to alter data as needed.


Code analysis:
1. Created check.js to select the checked option and using the combination of id and class, I was able to get the display Divisons.
2. Altered CSS only in few places to make sure that the page is consistent. And the only changes made were to improve the display the searched data.