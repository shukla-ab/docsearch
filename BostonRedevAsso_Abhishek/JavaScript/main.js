
var bra = {

    /**
    * Load Function
    */
    load: function(){
        //bra.LoadHomeMap();
        //bra.LoadInteriorMap();
        bra.LoadHomeSlider();
        bra.PauseonMousehover();
    },

    LoadHomeMap : function(){
        var url = '/map/index.html';
        var f = document.createElement('iframe');
        f.src = url;
        f.width = 1200;
        f.height = 723;
        f.scrolling = 'no';
        $('.homeMapIframe').html(f);
    },

    LoadInteriorMap : function(){
        var url = 'http://bra-map.herokuapp.com';
        var f = document.createElement('iframe');
        f.src = url;
        f.width = 805;
        f.height = 885;
        f.scrolling = 'no';
        $('.interiorMapIframe').html(f);
        //var thehtml = jQuery.ajax({
        //    url: '/maps',
        //    async: false
        //}).responseText;
        //$('.interiorMapIframe').html(thehtml);
    },

    LoadHomeSlider : function(){
        if ($(".slider .slide").length < 2)
        {
            $(".sliderNav").css("display", "none");

        }
    },


    
    
    PauseonMousehover: function () {

        $('.slide').mouseenter(function () {

           
            $('#imgSlide').cycle('pause');
           

            console.log("alert");
        });


    },
 

    /**
    * Init Function
    */
    init: function () {
        bra.DisableRightClickOnImage();
        bra.CSSHook();
        bra.HomeSlider();
        bra.Twitter();
        bra.EmailDocument();
        bra.EmailArticle();
        bra.EmailPublication();
        bra.SubscriptionCheckBox();
        bra.EventCalendarDateRange();
        bra.CommentCharacterCount();

        bra.CustomCheckBox();
        bra.DocSearchDateRange();
        bra.OptGroup();
        bra.Select2();
        bra.DocumentCenterFilter();
        
        bra.Masonry();
        bra.Glossary();
        bra.Analytics();
    },
    Analytics: function () {

        $("a[href*='/documents/']").on('click', function () {
            var action = this.href.substr(this.href.indexOf("documents/") + 10, this.href.length);
            var label = this.innerHTML;
            _gaq.push(['_trackEvent', 'Downloads', action.toLowerCase(), label]);
        });

        $("a[href*='mailto:']").on('click', function () {
            var action = this.href.substr(this.href.indexOf("mailto:") + 7, this.href.length);
            var label = this.innerHTML;
            _gaq.push(['_trackEvent', 'E-mail', action.toLowerCase(), label]);
        });

        $('a:not([href*="www.bostonredevelopmentauthority.org"]):not([href^="mailto:"]):not([href^="#"]):not([href^="/"]):not([href^="javascript:"]):not([href^="../"])').on('click', function () {
            var action = this.href;
            var label = this.innerHTML;
            _gaq.push(['_trackEvent', 'Outgoing', action.toLowerCase(), label]);
        });

        $(".calendarInfoAddition a[href*='javascript:']").on('click', function () {
            var action = $(this).parent('h2').attr('braevent:date');
            var label = $(this).parent('h2').attr('braevent:title');
            _gaq.push(['_trackEvent', 'Add To Calendar', action, label]);
        });

    },
    Glossary : function() {
        //$('article.columnOne').glossary('/utility/glossaryterms');
        $('.mainContent').glossary('/utility/glossaryterms', {
            ignorecase: true,
			showonce: true,
            excludetags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'a']            
        });

        $('body').bind('click', function (e) {
            if ($(e.target).closest('#glossaryContent').length == 0) {
                $('#glossaryContent').fadeOut("300");
            }
        });

    },

    CSSHook: function(){
        $('.ownedLandFilter .col:nth-of-type(5)').css("clear", "left");
        $('a.FeedLink').attr('target', '_blank');
    },
    
    DisableRightClickOnImage: function() {
        $('img').bind('contextmenu', function (e) {
            return false;
        });
    },

    CustomCheckBox: function() {
        $('.customCheckbox input').checkbox();
        $('.filterOptions').show();
    },

    CommentCharacterCount: function () {
        $(".formColumns .fullWidthLi textarea, .formColumns1 .fullWidthLi textarea").textcounter({
            max: 6000,
            countDownText: "",
            countDown: true,
            stopInputAtMaximum: true
        });

        $(".text-count-wrapper").addClass("textareaCaption");
    },

    SubscriptionCheckBox : function(){
        $("#chkAllNeighborhoods.selectAllNeighborhoods").on("change", function () {
            if (this.checked) {
                $(".neighborhoodCheckBoxList input[type='checkbox']").prop('checked', true);
            }
            else {
                $(".neighborhoodCheckBoxList input[type='checkbox']").prop('checked', false);
            }
        });

        $("#chkAllResearchPublications.selectAllResearchPublications").on("change", function () {
            if (this.checked) {
                $(".researchPublicationsCheckBoxList input[type='checkbox']").prop('checked', true);
            }
            else {
                $(".researchPublicationsCheckBoxList input[type='checkbox']").prop('checked', false);
            }
        });

        $("#chkAll.selectAll").on("change", function () {
            if (this.checked) {
                $("#chkAllNeighborhoods.selectAllNeighborhoods").prop('checked', true);
                $(".neighborhoodCheckBoxList input[type='checkbox']").prop('checked', true);
                $(".businessInitCheckBoxList input[type='checkbox']").prop('checked', true);
                $(".researchPublicationCheckBoxList input[type='checkbox']").prop('checked', true);
                $(".opPropCheckBoxList input[type='checkbox']").prop('checked', true);
            }
            else {
                $("#chkAllNeighborhoods.selectAllNeighborhoods").prop('checked', false);
                $(".neighborhoodCheckBoxList input[type='checkbox']").prop('checked', false);
                $(".businessInitCheckBoxList input[type='checkbox']").prop('checked', false);
                $(".researchPublicationCheckBoxList input[type='checkbox']").prop('checked', false);
                $(".opPropCheckBoxList input[type='checkbox']").prop('checked', false);
            }
        });
    },

    Twitter: function(){
        $(".twitterFeedLink").click(function (e) {
            e.preventDefault();
            $(this).siblings('.hidden').eq(0).toggle("fade", "300");
        });
    },

    HomeSlider: function () {
        
        $('.homeSlider .slide').hide();

        $(".openSlide").click(function (e) {
            e.preventDefault();
            $('.slide .slideImg').toggle("blind", 500, function () {
                var slide = $(this).parents('.slide');
                slide.find('.openSlide').toggleClass("closeSlide");
                if (!slide.find('.openSlide').hasClass('closeSlide'))
                    $(this).hide();
            });
        });

        // Document Center Expanded Box
        $(".expandBtn").click(function () {
            var $change = $(this);
            $(this).siblings('.toggleTD').toggle("blind", 500, function () {
                $change.toggleClass("open");
            });
        });

        $('.homeSlider .slide').hide();
        $('.homeSlider .slides').cycle({
            pager: '.homeSliderNav',
            cleartypeNoBg: true,
            fit: 1,
            speed: 1400,
            timeout:6000,
            delay: 2000,
            pagerEvent: 'click.cycle',
            pauseOnPagerHover: true,
            
        });
		
		// auto opens homepage slider
		//$('.slide .slideImg').show();
		//$('.openSlide').toggleClass("closeSlide");
		 
		    
    },   

    Masonry: function () {
        var $container = $('section:not(.planning_init) .infoListingWrapper');
        var $pagination = $('.paging');
        
        $container.hide();
        $pagination.hide();
        
        $('.loading').css('margin-left', '45%').append("<img src='/images/assets/loading.gif' alt='Loading...'/>");

        // initialize
        $container.imagesLoaded(function () {
            $('.loading').empty();
            $container.show().masonry({ columnWidth: 0, gutter: 14 });
            $pagination.show();
        });
    },

    OptGroup: function () {
        $("select.projectSearchFilter option[classification = 'Development Projects']").wrapAll("<optgroup label='Development Projects'>");
        $("select.projectSearchFilter option[classification = 'Capital Constructions']").wrapAll("<optgroup label='Capital Constructions'>");
        $("select.projectSearchFilter option[classification = 'Planning Initiatives']").wrapAll("<optgroup label='Planning Initiatives'>");
        $("select.projectSearchFilter option[classification = 'Institutional Master Plans']").wrapAll("<optgroup label='Institutional Master Plans'>");
    },

    Select2: function(){
        $(".autocompletedSelect").select2({ placeholder: "Select one or many" });
    },
    
    DocumentCenterFilter: function () {
        var filterDocType = bra.GetParameterByName("doctype");
        if (filterDocType != "") {
            $(".filterNav.documentTypeFilter").accordion({
                collapsible: true,
                heightStyle: "content"
            });
        }
        else {
            $(".filterNav.documentTypeFilter").accordion({
                collapsible: true,
                heightStyle: "content",
                active: false
            });
        }
        
        var filterNeighborhood = bra.GetParameterByName("neighborhood");
        if (filterNeighborhood != "") {
            $(".filterNav.neighborhoodFilter").accordion({
                collapsible: true,
                heightStyle: "content"
            });
        }
        else
        {
            $(".filterNav.neighborhoodFilter").accordion({
                collapsible: true,
                heightStyle: "content",
                active: false
            });
        }

        var filterProject = bra.GetParameterByName("project");
        if (filterProject != "") {
            $(".filterNav.projectFilter").accordion({
                collapsible: true,
                heightStyle: "content"
            });
        }
        else {
            $(".filterNav.projectFilter").accordion({
                collapsible: true,
                heightStyle: "content",
                active: false
            });
        }

        var filterProgram = bra.GetParameterByName("program");
        if (filterProgram != "") {
            $(".filterNav.programFilter").accordion({
                collapsible: true,
                heightStyle: "content"
            });
        }
        else {
            $(".filterNav.programFilter").accordion({
                collapsible: true,
                heightStyle: "content",
                active: false
            });
        }

        var filterDepartment = bra.GetParameterByName("department");
        if (filterDepartment != "") {
            $(".filterNav.departmentFilter").accordion({
                collapsible: true,
                heightStyle: "content"
            });
        }
        else {
            $(".filterNav.departmentFilter").accordion({
                collapsible: true,
                heightStyle: "content",
                active: false
            });
        }

        $(".filterNav-level2.collapse" ).accordion({
        	collapsible: true,
        	heightStyle: "content",
        	active: false
        });

        $(".filterNav-level2.expand").accordion({
            collapsible: true,
            heightStyle: "content"
        });
    },

    GetParameterByName: function(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },


    EmailPublication: function () {

        $("a.shareEmailListing").on("click", function (e) {
            e.preventDefault();

            var host = "http://" + window.location.hostname;
            var $section = $(this).parents(".researchPubTable");
            var title = "BRA - Research Publication: " + $section.find("caption h2").text();

            var content = "";
            $section.find(".documentLink").each(function () {
                content += escape(host + $(this).attr("href") + "\r");
            });

            window.location.href = "mailto:?subject=" + title + "&body=" + content;
        });

        $("a.shareEmailFeatured").on("click", function (e) {
            e.preventDefault();

            var host = "http://" + window.location.hostname;
            var $section = $(this).parent();
            var title = "BRA - Research Publication: " + $(this).parent().find("h1").text();
            
            var content = "";
            $section.find(".documentLink").each(function () {
                content += escape(host + $(this).attr("href") + "\r");
            });

            window.location.href = "mailto:?subject=" + title + "&body=" + content;
        });
    },

    EmailArticle : function(){
      $("a.email").on("click",function(){
        window.location.href="mailto:?subject="+document.title+"&body="+escape(window.location.href);
      });
    },

    EmailDocument: function () {
        $("a.emailLink.documentCenter").on("click", function (e) {
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var $section = $(this).parents(".documentTableWrapper");
            var title = "BRA - Document: " + $.trim($section.find("caption h2").text());

            var content = "";
            $section.find(".downloadLink").each(function () {
                content += escape($(this).attr("href") + "\r");
            });

            window.location.href = "mailto:?subject=" + title + "&body=" + content;
        });
    },

    EventCalendarDateRange: function () {
        $(".datepicker.eventCalendar").datepicker({
            showOn: "both",
            buttonImage: "images/datePicker.png",
            buttonImageOnly: true,
            onSelect: function (dateText, inst) {
                var date = new Date(dateText);
                var tmpDate = new Date(date);
                tmpDate.setDate(tmpDate.getDate() + 6);
                var endDate = new Date(tmpDate);

                var startDateText = $.datepicker.formatDate("M dd, yy", date);
                var endDateText = $.datepicker.formatDate("M dd, yy", endDate);

                $(this).val(startDateText + " - " + endDateText);

                $(".calendarFilter input.btnCalendarFilter").trigger('click');
            }
        });

        // Set the max height to eliminate the flicker.
        $(".calendarTable tr").each(function () {
            var _array = [];
            $(this).find("td").each(function () {
                _array.push($(this).height());
            });
            var max = Math.max.apply(Math, _array);
            $(this).find("td").each(function() {
                $(this).height(max);
            });
        });
    },
  
    DocSearchDateRange : function(){
      $( ".datepicker.from" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        showOn: "both",
        buttonImage: "images/datePicker.png",
        buttonImageOnly: true,
        onClose: function( selectedDate ) {
          $( ".datepicker.to" ).datepicker( "option", "minDate", selectedDate );
        }
      });
      $( ".datepicker.to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        showOn: "both",
        buttonImage: "images/datePicker.png",
        buttonImageOnly: true,
        onClose: function( selectedDate ) {
          $( ".datepicker.from" ).datepicker( "option", "maxDate", selectedDate );
        }
      });
    }
 }

$(window).load(function () {
    bra.load();
});

$(function () {
    bra.init();
});

$( ".MapGo" ).click(function() {
    location.reload();
    loadMap();
});

function loadMap(){
    var inst = $('[data-remodal-id=map]').remodal();
    setTimeout(inst.open() ,1000);
}